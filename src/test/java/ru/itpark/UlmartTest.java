package ru.itpark;

import domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.Ulmart;

import javax.naming.InvalidNameException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UlmartTest {
    private Ulmart ulmart;
    private Product computer = new Computer(1, "MXP", 42080, "Gaming computer", "Computers", "Intel Core i3-7100", " \n" +
            "NVIDIA GeForce GTX 1050 Ti");
    private Product smartphone = new Smartphone(2, "Prestigio", 4990, "Good smartphone", "Smartphones", 16, 13);
    private Product tv = new TV(3, "LG", 119990, "Modern TV", "TVs", "OLED");
    private Product tShirt = new TShirt(4, "Nike", 4999, "Sport T-Shirt", "Clothes", 52, "black");

    @BeforeEach
    void setUp() {
        ulmart = new Ulmart();
        ulmart.add(computer);
        ulmart.add(smartphone);
        ulmart.add(tv);
        ulmart.add(tShirt);


    }

    @Test
    public void testUlmart() {
        Assertions.assertNotNull(ulmart);
    }


    @Test
    public void testUlmartSize() {
        int size = ulmart.size();
        Assertions.assertEquals(4, size);
    }


    @Test
    public void AddSmartphone() {
        ulmart.add(new Smartphone(2, "Prestigio", 4990, "Good smartphone", "Smartphones", 16, 13));
        int size = ulmart.size();
        Assertions.assertEquals(5, size);
    }


    @Test
    public void noResultForNonExistenceName() {
        ArrayList<Product> products = ulmart.findByName("Reebok");
        Assertions.assertEquals(0, products.size());
    }

    @Test
    public void oneResultForExistentName() {
        ArrayList<Product> products = ulmart.findByName("Nike");
        Assertions.assertEquals(1, products.size());
    }

    @Test
    public void sortByPriceTest() {
        List<Product> autos = ulmart.sortByPrice();
        List<Product> sorted = new ArrayList<Product> ();
        sorted.add(tShirt);
        sorted.add(smartphone);
        sorted.add(computer);
        sorted.add(tv);
        assertEquals(sorted, autos);}


    @Test
    public void noResultForNonExistentCategory() {
        ArrayList<Product> products = ulmart.viewByCategory("Phones");
        Assertions.assertEquals(0, products.size());
    }

    @Test
    public void oneResultForExistentCategory() {
        ArrayList<Product> products = ulmart.viewByCategory("Computers");
        Assertions.assertEquals(1, products.size());
    }

    @Test
    public void oneResult() {
        ArrayList<Smartphone> result = ulmart.getSmartphones();
        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void InvalidNameExceptionTest() {
        Assertions.assertThrows(
                InvalidNameException.class,
                () -> {
                    ulmart.findByName("Invalid");
                });
    }
}

