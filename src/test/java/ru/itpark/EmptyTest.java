package ru.itpark;

import domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.Ulmart;

import javax.naming.InvalidNameException;
import java.util.ArrayList;

public class EmptyTest {
    private Ulmart ulmart;

    @BeforeEach
    void setUp() {
        ulmart = new Ulmart();
    }

    @Test
    public void testUlmart() {
        Assertions.assertNotNull(ulmart);
    }


    @Test
    public void testUlmartSize() {
        int size = ulmart.size();
        Assertions.assertEquals(0, size);
    }

    @Test
    public void AddSmartphone() {
        ulmart.add(new Smartphone(2, "Prestigio", 4990, "Good smartphone", "Smartphones", 16, 13));
        int size = ulmart.size();
        Assertions.assertEquals(1, size);
    }

    @Test
    public void AddComputer() {
        ulmart.add(new Computer(1, "MXP", 42080, "Gaming computer", "Computers", "Intel Core i3-7100", " \n" +
                "NVIDIA GeForce GTX 1050 Ti"));
        int size = ulmart.size();
        Assertions.assertEquals(1, size);
    }

    @Test
    public void AddTV() {
        ulmart.add(new TV(3, "LG", 119990, "Modern TV", "TVs", "OLED"));
        int size = ulmart.size();
        Assertions.assertEquals(1, size);
    }

    @Test
    public void AddTShirt() {
        ulmart.add(new TShirt(4, "Nike", 4999, "Sport T-Shirt", "Clothes", 52, "black"));
        int size = ulmart.size();
        Assertions.assertEquals(1, size);
    }

    @Test
    public void testFindByName() {
        ArrayList<Product> products = ulmart.findByName("Nike");
        Assertions.assertEquals(0, products.size());
    }

    @Test
    public void InvalidNameExceptionTest() {
        Assertions.assertThrows(
                InvalidNameException.class,
                () -> {
                    ulmart.findByName("Invalid");
                });
    }

        @Test
        public void testViewByCategory () {
            ArrayList<Product> products = ulmart.viewByCategory("Smartphones");
            Assertions.assertEquals(0, products.size());
        }
}
