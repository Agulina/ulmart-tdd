package ru.itpark;

import domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.Ulmart;

import javax.naming.InvalidNameException;
import java.util.ArrayList;

public class UlmartWithOneItemtest {
    private Ulmart ulmart;
    private Product product;

    @BeforeEach
    void setUp() {
        ulmart = new Ulmart();
        ulmart.add(new Computer(1, "MXP", 42080, "Gaming computer", "Computers", "Intel Core i3-7100", " \n" +
                "NVIDIA GeForce GTX 1050 Ti"));
    }

    @Test
    public void testUlmart() {
        Assertions.assertNotNull(ulmart);
    }


    @Test
    public void testUlmartSize() {
        int size = ulmart.size();
        Assertions.assertEquals(1, size);
    }

    @Test
    public void AddSmartphone() {
        ulmart.add(new Smartphone(2, "Prestigio", 4990, "Good smartphone", "Smartphones", 16, 13));
        int size = ulmart.size();
        Assertions.assertEquals(2, size);
    }

    @Test
    public void MultipleAdding() {
        ulmart.add(new TV(3, "LG", 119990, "Modern TV", "TVs", "OLED"));
        ulmart.add(new TShirt(4, "Nike", 4999, "Sport T-Shirt", "Clothes", 52, "black"));

        int size = ulmart.size();
        Assertions.assertEquals(3, size);
    }

    @Test
    public void noResultForNonExistenceName() {
        ArrayList<Product> products = ulmart.findByName("Reebok");
        Assertions.assertEquals(0, products.size());
    }

    @Test
    public void oneResultForExistentName() {
        ArrayList<Product> products = ulmart.findByName("MXP");
        Assertions.assertEquals(1, products.size());
    }

    @Test
    public void noResultForNonExistentCategory() {
        ArrayList<Product> products = ulmart.viewByCategory("Phones");
        Assertions.assertEquals(0, products.size());
    }

    @Test
    public void oneResultForExistentCategory() {
        ArrayList<Product> products = ulmart.viewByCategory("Computers");
        Assertions.assertEquals(1, products.size());
    }

    @Test
    public void InvalidNameExceptionTest() {
        Assertions.assertThrows(
                InvalidNameException.class,
                () -> {
                    ulmart.findByName("Invalid");
                });
    }
}
