package domain;

public class TShirt extends Product {
    private int size;
    private String color;

    public TShirt(long id, String name, int price, String description, String category, int size, String color) {
        super(id, name, price, description, category);
        this.size = size;
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int compareTo(Product o) {
        return 0;
    }
}
