package domain;


public class Smartphone extends Product {
    private int gb;
    private int camera;

    public Smartphone(long id, String name, int price, String description, String category, int gb, int camera) {
        super(id, name, price, description, category);
        this.gb = gb;
        this.camera = camera;
    }

    public int getGb() {
        return gb;
    }

    public void setGb(int gb) {
        this.gb = gb;
    }

    public int getCamera() {
        return camera;
    }

    public void setCamera(int camera) {
        this.camera = camera;
    }

    @Override
    public int compareTo(Product o) {
        return 0;
    }


}

