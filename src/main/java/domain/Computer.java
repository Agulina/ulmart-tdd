package domain;

public class Computer extends Product {
    private String cpu;
    private String videoCard;

    public Computer(long id, String name, int price, String description, String category, String cpu, String videoCard) {
        super(id, name, price, description, category);
        this.cpu = cpu;
        this.videoCard = videoCard;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(String videoCard) {
        this.videoCard = videoCard;
    }

    @Override
    public int compareTo(Product o) {
        return 0;
    }
}

