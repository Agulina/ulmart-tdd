package domain;

public class TV extends Product {
    private String type;

    public TV(long id, String name, int price, String description, String category, String type) {
        super(id, name, price, description, category);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(Product o) {
        return 0;
    }
}

