package service;

import domain.Product;
import domain.Smartphone;

import java.util.ArrayList;
import java.util.Collections;
import ru.itpark.exception.InvalidNameException;

public class Ulmart {
    private ArrayList<Product> products = new ArrayList<Product>();

    public int size() {
        return products.size();
    }

    public void add(Product product) {
        products.add(product);
    }

    public ArrayList<Product> findByName(String name) {
        ArrayList<Product> result = new ArrayList<Product>();

        for (Product product : products) {
            String productName = product.getName();
            if (productName.contains(name)) {
                result.add(product);
            }
        }

        if (result.size() == 0) {
            throw new InvalidNameException();
        }

        return result;
    }


    public ArrayList<Product> sortByPrice() {
        ArrayList<Product> result = new ArrayList<Product>();


        for (Product product : products) {
            result.add(product);
        }
        Collections.sort(result);
        return result;
    }

    public ArrayList<Product> viewByCategory(String category) {
        ArrayList<Product> result = new ArrayList<Product>();

        for (Product product : products) {
            String productCategory = product.getCategory();
            if (productCategory.contains(category)) {
                result.add(product);
            }
        }
        return result;
    }

    public ArrayList<Smartphone> getSmartphones() {
        ArrayList<Smartphone> result = new ArrayList<Smartphone>();

        for (Product product : products) {
            if (product instanceof Smartphone) {
                result.add((Smartphone) product);
            }
        }
        return result;
    }
}
